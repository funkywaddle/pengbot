# PENGBOT.COM #

This is the website for the configuration of PengBot, a Twitch Chat Bot.
It is written in Python using the Django framework.

### How do I get set up? ###

* Clone the repo
* ```python -m pip install -r requirements.txt```
* ```cp sample.env .env```
* Add values to environment variables in .env file
    * TWITCH_* values come from your twitch app configs on [Twitch Developers](https://dev.twitch.tv/) 
    * Leave DATABASE_* values empty for default sqlite3 values